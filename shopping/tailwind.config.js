/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      navBg: "#e0e7ff",
      navColor: "#7c3aed",
      activeNav: "#3730a3",
      activNavBg: "#f6f6f6",
      cartbg: "#ffff",
      textColor: "#8b5cf6",
      borderColor: "#6366f1",
    },
    extend: {},
  },
  plugins: [],
};
