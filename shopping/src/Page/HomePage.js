import Layout from "../Layout/Layout";
import http from "../Services/HttpServices";
import { useCart, useCartAction } from "../Provider/CartProvider";
import { CheckInCart } from "../Utils/CheckInCart";
import { toast } from "react-toastify";
import { useEffect, useState } from "react";
import * as data from "../data";

const HomePage = () => {
  const [products, setProduc] = useState([]);
  const dispatch = useCartAction();

  const { cart } = useCart();

  useEffect(() => {
    http
      .get("/product")
      .then((result) => {
        setProduc(result.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const addProductHandler = (product) => {
    toast.success(`${product.name} added to cart`);
    dispatch({ type: "INCREMENT_TO_CART", payload: product });
  };

  return (
    <Layout>
      <main>
        <section className="grid grid-cols-4 gap-4 pt-20 px-5 md:p-20 mx-auto">
          {products &&
            products.map((productItem, index) => (
              <div
                className="col-span-2 md:col-span-1 bg-cartbg p-3 rounded-lg max-w-7xl"
                key={index}
              >
                <img
                  src={productItem.image}
                  alt={productItem.name}
                  className="h-44 w-full"
                />
                <div className="flex flex-col md:flex-row md:justify-between p-4">
                  <h2 className="text-textColor text-sm">{productItem.name}</h2>
                  <span className="text-textColor">{productItem.price}$</span>
                  <button
                    className="border-2 px-3 rounded-md border-borderColor text-textColor"
                    onClick={() => addProductHandler(productItem)}
                  >
                    {CheckInCart(cart, productItem) ? "In Cart" : "Add To Cart"}
                  </button>
                </div>
              </div>
            ))}
        </section>
      </main>
    </Layout>
  );
};

export default HomePage;
