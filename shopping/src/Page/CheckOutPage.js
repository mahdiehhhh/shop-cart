import Layout from "../Layout/Layout";
import { useAuth } from "../Provider/AuthProvider";
const CheckOutPage = () => {
  const Auth = useAuth();
  console.log("Auth", Auth);
  return (
    <Layout>
      <h2 className="pt-20">CheckOutPage</h2>

      <div className="mt-5">
        <p className="mb-3">name: {Auth.name}</p>
        <p className="mb-3">email: {Auth.email}</p>
        <p className="mb-3">phoneNumber: {Auth.phoneNumber}</p>
      </div>
    </Layout>
  );
};

export default CheckOutPage;
