import CartSummery from "../components/CartSummery";
import Layout from "../Layout/Layout";
import { useCart, useCartAction } from "../Provider/CartProvider";

const Cart = () => {
  const { cart } = useCart();

  const dispatch = useCartAction();

  const incHandler = (cartItem) => {
    dispatch({ type: "INCREMENT_TO_CART", payload: cartItem });
  };

  const decHandler = (cartItem) => {
    dispatch({ type: "DECREMENT_TO_CART", payload: cartItem });
  };

  return (
    <Layout>
      <div className="pt-24">
        {cart.length ? (
          <div className="px-36">
            <div className="grid grid-cols-5 gap-5">
              <div className="col-span-3 p-3 bg-[#ffff] rounded-xl">
                {cart.map((item, index) => (
                  <div
                    className="flex justify-between items-center p-2 border-b border-[#cccc]"
                    key={index}
                  >
                    <img
                      src={item.image}
                      alt={item.name}
                      className="w-40 h-40"
                    />
                    <h2 className="text-textColor">{item.name}</h2>
                    <span className="text-textColor">
                      {item.price * item.quantity}$
                    </span>

                    <div>
                      <button
                        onClick={() => decHandler(item)}
                        className="border-2 px-3 rounded-md border-borderColor text-textColor"
                      >
                        -
                      </button>

                      <span className="text-textColor mx-3">
                        {item.quantity}
                      </span>

                      <button
                        onClick={() => incHandler(item)}
                        className="border-2 px-3 rounded-md border-borderColor text-textColor"
                      >
                        +
                      </button>
                    </div>
                  </div>
                ))}
              </div>
              <CartSummery />
            </div>
          </div>
        ) : (
          <p>cart is empty</p>
        )}
      </div>
    </Layout>
  );
};

export default Cart;
