const Input = ({ name, label, formik, type }) => {
  return (
    <div className="formControl">
      <label htmlFor={name}>{label}</label>

      <input
        id={name}
        type={type}
        name="name"
        {...formik.getFieldProps(name)}
        className="outline-none border-2 border-textColor rounded-lg mt-3 "
      />

      {formik.errors[name] && formik.touched[name] && (
        <div className="text-[#cccc]">{formik.errors[name]}</div>
      )}
    </div>
  );
};

export default Input;
