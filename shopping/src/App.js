import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Cart from "./Page/Cart";
import HomePage from "./Page/HomePage";
import CartProvider from "./Provider/CartProvider";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CheckOutPage from "./Page/CheckOutPage";
import LoginPage from "./Page/LoginPage";
import SignUpPage from "./Page/SignUpPage";
import AuthProvider from "./Provider/AuthProvider";
import Profile from "./components/Profile";

function App() {
  return (
    <AuthProvider>
      <CartProvider>
        <div className="App">
          <ToastContainer />
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<HomePage />} />
              <Route path="/login" element={<LoginPage />} />
              <Route path="/signup" element={<SignUpPage />} />
              <Route path="/checkout" element={<CheckOutPage />} />
              <Route path="/profile" element={<Profile />} />
              <Route path="/cart" element={<Cart />} />
            </Routes>
          </BrowserRouter>
        </div>
      </CartProvider>
    </AuthProvider>
  );
}

export default App;
