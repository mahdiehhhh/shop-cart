import { createContext, useContext, useReducer } from "react";

import cartReducer from "./cartReducer";

const cartContext = createContext();

const cartContextDispatch = createContext();

const initialState = {
  cart: [],
  total: 0,
};

const CartProvider = ({ children }) => {
  const [cart, dispatch] = useReducer(cartReducer, initialState);
  return (
    <cartContext.Provider value={cart}>
      <cartContextDispatch.Provider value={dispatch}>
        {children}
      </cartContextDispatch.Provider>
    </cartContext.Provider>
  );
};

export default CartProvider;

export const useCart = () => useContext(cartContext);
export const useCartAction = () => useContext(cartContextDispatch);
