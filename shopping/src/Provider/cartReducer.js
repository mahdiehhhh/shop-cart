const addProductToCart = (state, payload) => {
  const updatedCart = [...state.cart];
  const updatedItemIndex = updatedCart.findIndex(
    (item) => item._id === payload._id
  );

  if (updatedItemIndex < 0) {
    updatedCart.push({ ...payload, quantity: 1 });
  } else {
    const updatedItem = { ...updatedCart[updatedItemIndex] };

    updatedItem.quantity++;

    updatedCart[updatedItemIndex] = updatedItem;
  }
  return {
    ...state,
    cart: updatedCart,
    total: state.total + payload.price,
  };
};

const removeProductFromCart = (state, payload) => {
  const updatedCart = [...state.cart];

  const updatedItemIndex = updatedCart.findIndex(
    (item) => item._id === payload._id
  );
  const updatedItem = { ...updatedCart[updatedItemIndex] };

  if (updatedItem.quantity === 1) {
    const filterCart = updatedCart.filter((item) => item._id !== payload._id);
    return {
      ...state,
      cart: filterCart,
      total: state.total - payload.price,
    };
  } else {
    updatedItem.quantity--;

    updatedCart[updatedItemIndex] = updatedItem;

    return {
      ...state,
      cart: updatedCart,
      total: state.total - payload.price,
    };
  }
};

const cartReducer = (state, action) => {
  switch (action.type) {
    case "INCREMENT_TO_CART":
      return addProductToCart(state, action.payload);

    case "DECREMENT_TO_CART":
      return removeProductFromCart(state, action.payload);
    default:
      return state;
  }
};

export default cartReducer;
