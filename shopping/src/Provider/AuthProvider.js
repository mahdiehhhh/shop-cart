import { createContext, useContext, useEffect, useState } from "react";
const AuthContext = createContext();
const AuthContextDispatch = createContext();

const AuthProvider = ({ children }) => {
  const [stateAuth, setstateAuth] = useState(false);

  useEffect(() => {
    const userDataInfo = JSON.parse(localStorage.getItem("authState")) || false;
    if (userDataInfo) {
      setstateAuth(userDataInfo);
    }
  }, []);

  useEffect(() => {
    const dataInfo = JSON.stringify(stateAuth);
    localStorage.setItem("authState", dataInfo);
  }, [stateAuth]);

  return (
    <AuthContext.Provider value={stateAuth}>
      <AuthContextDispatch.Provider value={setstateAuth}>
        {children}
      </AuthContextDispatch.Provider>
    </AuthContext.Provider>
  );
};

export default AuthProvider;

export const useAuth = () => useContext(AuthContext);
export const useAuthAction = () => useContext(AuthContextDispatch);
