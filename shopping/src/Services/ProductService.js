import http from "./HttpServices";

export const showProduct = () => {
  return http.get("/product");
};
