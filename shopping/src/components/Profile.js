import Layout from "../Layout/Layout";
import { useAuth } from "../Provider/AuthProvider";

const Profile = () => {
  const profileData = useAuth();
  return (
    <Layout>
      {profileData && (
        <div className="p-22 text-left">
          <p className="text-textColor">email: {profileData.name}</p>
          <p className="text-textColor">email: {profileData.email}</p>
          <p className="text-textColor">email: {profileData.phoneNumber}</p>
        </div>
      )}
    </Layout>
  );
};

export default Profile;
