import { Link, NavLink } from "react-router-dom";
import { useCart } from "../../Provider/CartProvider";
import logo from "../../logoshoees.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSignInAlt,
  faShoppingCart,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { useAuth } from "../../Provider/AuthProvider";

const Navigation = () => {
  const { cart } = useCart();
  const userData = useAuth();
  return (
    <header>
      <nav className="py-3 px-8 bg-navBg flex justify-between items-center ">
        <div className="flex items-center gap-10">
          <Link
            to={userData ? "/profile" : "/login"}
            className="flex items-center text-textColor text-sm font-bold"
          >
            {userData ? (
              <span>
                <FontAwesomeIcon icon={faUser} className="mr-2" />
                Profile
              </span>
            ) : (
              <span>
                <FontAwesomeIcon icon={faSignInAlt} className="mr-2" />
                Login/SignUp
              </span>
            )}
          </Link>

          <NavLink
            to="/cart"
            className={({ isActive }) =>
              isActive
                ? "text-activeNav font-bold bg-activNavBg p-1.5 rounded-lg flex items-center"
                : "text-navColor font-bold flex items-center p-1.5 "
            }
          >
            <span>
              <FontAwesomeIcon icon={faShoppingCart} />
            </span>
            <p className="bg-[#cccc] w-5 text-textColor rounded-full font-bold text-xs absolute top-4 ml-3">
              {cart.length}
            </p>
          </NavLink>
        </div>

        <div className="flex items-center gap-11">
          <NavLink
            to="/"
            className={({ isActive }) =>
              isActive
                ? "text-activeNav font-bold bg-activNavBg p-2 rounded-lg"
                : "text-navColor font-bold p-2 "
            }
          >
            Home
          </NavLink>
          <img src={logo} alt="Logo" className="w-12 rounded-full" />
        </div>
      </nav>
    </header>
  );
};

export default Navigation;
