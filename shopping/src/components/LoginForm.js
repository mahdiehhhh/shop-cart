import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import * as Yup from "yup";
import Input from "../Common/Input";
import { useAuth, useAuthAction } from "../Provider/AuthProvider";
import { loginUser } from "../Services/LoginServices";

const initialValues = {
  email: "",
  password: "",
};

const validationSchema = Yup.object({
  email: Yup.string()
    .required("Email is require")
    .email("Invalid Email format"),

  password: Yup.string().required("Password is require"),
});

const LoginForm = () => {
  const [Error, setError] = useState(null);

  const [searchParams] = useSearchParams();

  const redirectUrl = searchParams.get("redirect") || "/";

  let histoy = useNavigate();

  const Auth = useAuth();

  useEffect(() => {
    if (Auth) {
      histoy(redirectUrl);
    }
  }, [Auth, histoy, redirectUrl]);

  const setAuth = useAuthAction();

  const onSubmit = async (value) => {
    try {
      const { data } = await loginUser(value);
      setAuth(data);
      histoy(redirectUrl);
      setError(null);
    } catch (err) {
      if (err.response && err.response.data.message) {
        setError(err.response.data.message);
      }
    }
  };
  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
    enableReinitialize: true,
    validateOnMount: true,
  });
  return (
    <div className="pt-32">
      <form
        onSubmit={formik.handleSubmit}
        className="bg-[#ffff] p-5 w-96 rounded-lg text-left mx-auto"
      >
        <Input formik={formik} name="email" label="Email: " type="email" />
        <Input
          formik={formik}
          name="password"
          label="Password: "
          type="password"
        />
        <button
          className={
            !formik.isValid
              ? "w-full mt-5 bg-[#cccc] text-[#ffff] p-1 rounded-lg cursor-not-allowed"
              : "w-full mt-5 bg-textColor text-[#ffff] p-1 rounded-lg"
          }
          type="submit"
          disabled={!formik.isValid}
        >
          Login
        </button>
        {Error && <p className="mt-3 text-[#642626]">{Error}</p>}
        <Link to={`/signup?redirect=${redirectUrl}`}>
          <p className="mt-3 text-textColor text-left">Not Signup Yet?</p>
        </Link>
      </form>
    </div>
  );
};

export default LoginForm;
