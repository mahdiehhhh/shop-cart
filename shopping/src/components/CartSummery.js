import { Link } from "react-router-dom";
import { useCart } from "../Provider/CartProvider";

const CartSummery = () => {
  const { cart, total } = useCart();

  const originalTotalPrice = cart.length
    ? cart.reduce((acc, curr) => acc + curr.quantity * curr.price, 0)
    : 0;
  return (
    <div className="col-span-2 p-5 bg-[#ffff] rounded-xl">
      <h2>card summery</h2>
      <div className="flex justify-between items-center mt-5 ">
        <span className="text-left text-textColor font-bold">Cart Total</span>
        <span className="text-left text-textColor font-bold">
          {originalTotalPrice} $
        </span>
      </div>

      <div className="flex justify-between items-center mt-5 border-b pb-3 border-[#cccc]">
        <span className="text-left text-textColor font-bold">Discount</span>
        <span className="text-left text-textColor font-bold">
          {originalTotalPrice - total}
        </span>
      </div>

      <div className="flex justify-between items-center mt-5">
        <span className="text-left text-textColor font-bold">Net Price</span>
        <span className="text-left text-textColor font-bold">{total}</span>
      </div>

      <Link to="/signup?redirect=/checkout">
        <button className="w-full mt-5 bg-textColor text-[#ffff] p-1 rounded-lg">
          checkout
        </button>
      </Link>
    </div>
  );
};

export default CartSummery;
