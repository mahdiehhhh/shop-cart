import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import * as Yup from "yup";
import Input from "../Common/Input";
import { useAuth, useAuthAction } from "../Provider/AuthProvider";
import { signupUser } from "../Services/SignupService";

const initialValues = {
  name: "",
  email: "",
  password: "",
  passwordConfirm: "",
  PhoneNumber: "",
};

const validationSchema = Yup.object({
  name: Yup.string()
    .required("Name is require")
    .min(6, "name should be 6 charecter"),

  email: Yup.string()
    .required("Email is require")
    .email("Invalid Email format"),

  phoneNumber: Yup.string().required("phoneNumber is require"),
  // .matches(/^[0-9]{11}$/),

  password: Yup.string().required("Password is require"),
  // .matches(
  //   "^(?=.*[A-Za-z])(?=.*d)(?=.*[@$!%*#?&])[A-Za-zd@$!%*#?&]{8,}$",
  //   "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
  // )
  passwordConfirm: Yup.string()
    .required("password Confirm is require")
    .oneOf([Yup.ref("password"), null], "Passwords must match"),
});
const SignupForm = () => {
  const [searchParams] = useSearchParams();

  const redirectUrl = searchParams.get("redirect") || "/";

  let Navigate = useNavigate();

  const setAuth = useAuthAction();
  
  const [Error, setError] = useState(null);

  const Auth = useAuth();

  useEffect(() => {
    if (Auth) {
      Navigate(redirectUrl);
    }
  }, [Auth, Navigate, redirectUrl]);

  const onSubmit = async (userInfo) => {
    const { email, name, password, phoneNumber } = userInfo;
    const userData = {
      email,
      name,
      password,
      phoneNumber,
    };

    try {
      const { data } = await signupUser(userData);
      setAuth(data);
      // localStorage.setItem("authState", JSON.stringify(data));
      setError(null);

      Navigate(redirectUrl);
    } catch (err) {
      console.log(err.response.data.message);
      if (err.response.data.message) {
        setError(err.response.data.message);
      }
    }
  };

  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
    validateOnMount: true,
  });

  return (
    <div className="pt-32">
      <form
        onSubmit={formik.handleSubmit}
        className="bg-[#ffff] p-5 w-96 rounded-lg text-left mx-auto"
      >
        <Input formik={formik} name="name" label="Name: " type="text" />
        <Input formik={formik} name="email" label="Email: " type="email" />
        <Input
          formik={formik}
          name="phoneNumber"
          label="Phone Number: "
          type="tel"
        />
        <Input
          formik={formik}
          name="password"
          label="Password: "
          type="password"
        />
        <Input
          formik={formik}
          name="passwordConfirm"
          label="Password Confirme: "
          type="password"
        />
        <button
          className={
            !formik.isValid
              ? "w-full mt-5 bg-[#cccc] text-[#ffff] p-1 rounded-lg cursor-not-allowed"
              : "w-full mt-5 bg-textColor text-[#ffff] p-1 rounded-lg"
          }
          type="submit"
          disabled={!formik.isValid}
        >
          Signup
        </button>

        {Error && <p className="mt-3 text-[#642626]">{Error}</p>}

        <Link to={`/login?redirect=${redirectUrl}`}>
          <p className="mt-3 text-textColor text-left">Already Login?</p>
        </Link>
      </form>
    </div>
  );
};

export default SignupForm;
